package com.tk1.game.model;

import java.util.Observable;

import com.tk1.game.vo.Coordinate;

public class CoordinateModel extends Observable{
	
	
	private Coordinate coordinate;
	
	public CoordinateModel(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	
	public Coordinate getCoordinate() {
		return coordinate;
	}
	
	public void setCoordinate(Coordinate coordinate){
		this.coordinate=coordinate;
		setChanged();
		notifyObservers(this.coordinate);
	}

}
