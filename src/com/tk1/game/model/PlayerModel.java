
package com.tk1.game.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import com.tk1.game.vo.Player;

public class PlayerModel extends Observable  {
	
	private List<Player> players;
	
	public PlayerModel(){
		this.players = new ArrayList<>();
	}
	
	public List<Player> getPlayers(){
		return Collections.unmodifiableList(this.players);
	}
	
	public void addPlayer(Player player){
		this.players.add(player);
		setChanged();
		notifyObservers(this.players);
	}
	
	public void addPlayers(List<Player> players){
		this.players.addAll(players);
		setChanged();
		notifyObservers(this.players);
	}
	
	public void clearAndAddPlayer(List<Player> players){
		this.players.clear();
		this.players.addAll(players);
		setChanged();
		notifyObservers(this.players);	
	}
	
}
