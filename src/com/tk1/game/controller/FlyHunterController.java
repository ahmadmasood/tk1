package com.tk1.game.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.Naming;
import java.util.List;

import com.tk1.game.client.GameClient;
import com.tk1.game.model.CoordinateModel;
import com.tk1.game.model.PlayerModel;
import com.tk1.game.server.IGameServer;
import com.tk1.game.view.FlyHunterView;
import com.tk1.game.vo.Coordinate;
import com.tk1.game.vo.Player;

public class FlyHunterController {

	private FlyHunterView flyHunterView;
	private PlayerModel playerModel;
	private CoordinateModel coordinateModel;
	private IGameServer gameServer;
	private GameClient gameClient;



	public FlyHunterController(FlyHunterView flyHunterView, PlayerModel playerModel, CoordinateModel coordinateModel){
		this.flyHunterView=flyHunterView;
		this.playerModel = playerModel;
		this.coordinateModel = coordinateModel;
	}

	private void connect(){
		if(flyHunterView.getPlayerName().isEmpty()){
			flyHunterView.showMessage("Please Enter Player Name to Connect!");
			return;
		}
		try {
			flyHunterView.showInConsole("Connecting...");			
			gameClient = new GameClient(this);
			gameServer = (IGameServer) Naming.lookup("rmi://localhost/GameServer");
			gameServer.login(gameClient,flyHunterView.getPlayerName());
			flyHunterView.showInConsole("Connected Successfuly...");
			flyHunterView.disablePlayerNameInput();
			flyHunterView.setConnectButtonText("Disconnect");
			flyHunterView.setConnectButtonActionCommand("disconnect");
			flyHunterView.setFlyVisibility(true);
		} catch (Exception exception) {
			flyHunterView.showMessage(exception.getMessage());
			exception.printStackTrace();
			flyHunterView.showInConsole("Error occured...");
		}

	}


	private void disconnect(){
		flyHunterView.showInConsole("Disconnecting...");
		try {
			gameServer.logout(flyHunterView.getPlayerName());
			gameClient=null;
			gameServer=null;
			flyHunterView.showInConsole("Disconnected Successfuly...");
			flyHunterView.enablePlayerNameInput();
			flyHunterView.setConnectButtonText("Connect");
			flyHunterView.setConnectButtonActionCommand("connect");
			flyHunterView.setFlyVisibility(false);
		} catch (Exception exception) {
			flyHunterView.showMessage(exception.getMessage());
			flyHunterView.showInConsole("Error occured...");
		}	
	}

	public ActionListener getFlyHuntAction(){
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					gameServer.huntFly(flyHunterView.getPlayerName());
				} catch (Exception exception) {
					flyHunterView.showMessage(exception.getMessage());
				}				
			}
		};

	}


	public ActionListener getConnectButtonAction(){
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if("connect".equals(e.getActionCommand())){
					connect();
					return;
				}
				disconnect();
			}
		};
		
	}
	
	public WindowAdapter getWindowCloseHandler(){
		return new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent){
				disconnect();
				System.exit(0);
			}        
		};
	}

	public void updateFly(Coordinate coordinate){
		System.out.println(coordinate);
		coordinateModel.setCoordinate(coordinate);
	}

	public void updateScore(List<Player> players){
		System.out.println(players);
		playerModel.clearAndAddPlayer(players);
	}

}
