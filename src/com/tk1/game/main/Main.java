package com.tk1.game.main;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;

import com.tk1.game.controller.FlyHunterController;
import com.tk1.game.model.CoordinateModel;
import com.tk1.game.model.PlayerModel;
import com.tk1.game.server.GameServerDriver;
import com.tk1.game.view.FlyHunterView;
import com.tk1.game.vo.Coordinate;

public class Main {

	public static void main(String[] args) {

		if(null!= args && args.length>=1){
			if( "client".equals(args[0])){
				System.out.println("Starting the Game...");
				FlyHunterView flyHunterView = new FlyHunterView(); // Creating client window
				if (args.length > 1) {
					try {
						//Setting client window initial coordinates  
						flyHunterView.setLocation(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
					} catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
						ex.printStackTrace();
					}
				}
				CoordinateModel coordinateModel = new CoordinateModel(new Coordinate(0, 0));
				coordinateModel.addObserver(flyHunterView); //Binding client view to CoordinateModel through Observer pattern

				PlayerModel playerModel = new PlayerModel();
				playerModel.addObserver(flyHunterView); //Binding client view to PlayerModel through Observer pattern

				FlyHunterController controller = new FlyHunterController(flyHunterView, playerModel, coordinateModel);
				flyHunterView.addHandlers(controller);
				flyHunterView.setVisible(true);
				System.out.println("Game Started");
			}else if("server".equals(args[0])){
				try {
					GameServerDriver.Start(); // Starting RMI server.
				} catch (MalformedURLException | RemoteException
						| AlreadyBoundException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
