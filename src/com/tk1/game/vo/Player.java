package com.tk1.game.vo;

import java.io.Serializable;

public class Player implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String playerName;
	private int score;
	
	public Player(String playerName){
		this.playerName=playerName;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void incrementScore(){
		this.score++;
	}

	@Override
	public String toString() {
		return "Player [playerName=" + playerName + ", score=" + score + "]";
	}
}
