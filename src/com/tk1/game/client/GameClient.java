package com.tk1.game.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import com.tk1.game.controller.FlyHunterController;
import com.tk1.game.vo.Coordinate;
import com.tk1.game.vo.Player;

public class GameClient extends UnicastRemoteObject implements IGameClient{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private FlyHunterController flyHunterController;
	
	public GameClient(FlyHunterController flyHunterController) throws RemoteException {
		this.flyHunterController=flyHunterController;
	}

	@Override
	public void refreshFlyPosition(int x, int y) throws RemoteException {
		flyHunterController.updateFly(new Coordinate(x, y));
	}

	@Override
	public void refreshScore(List<Player> players) throws RemoteException {
		flyHunterController.updateScore(players);
	}
	
}
