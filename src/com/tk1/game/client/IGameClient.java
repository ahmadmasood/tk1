package com.tk1.game.client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import com.tk1.game.vo.Player;

public interface IGameClient extends Remote {
	void refreshFlyPosition(int x, int y) throws RemoteException;
	void refreshScore(List<Player> players) throws RemoteException;
}
