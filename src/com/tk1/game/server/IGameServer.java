package com.tk1.game.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.tk1.game.client.IGameClient;

public interface IGameServer extends Remote{
	
	void login(IGameClient gameClient, String player) throws RemoteException;
	void huntFly(String player) throws RemoteException;
	void logout(String player) throws RemoteException;
}
