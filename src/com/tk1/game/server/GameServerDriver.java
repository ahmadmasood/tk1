package com.tk1.game.server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class GameServerDriver {
	public static void Start() throws MalformedURLException, RemoteException, AlreadyBoundException{
		final Object monitor = new Object();
		new Thread(new Runnable() {
			public void run() {
				try {
					LocateRegistry.createRegistry(1099);
					synchronized (monitor) {
						monitor.wait();                        
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("RMI Registry Thread finished.");
			}
		}, "RMI Registry Thread").start();
		System.out.print("Server is running.....");

		GameServer gameServer = new GameServer();
//		new Thread(gameServer).start();
		Naming.bind("GameServer", gameServer);
	}
}
