package com.tk1.game.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import com.tk1.game.client.IGameClient;
import com.tk1.game.vo.Player;

public class GameServer extends UnicastRemoteObject implements IGameServer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Map<IGameClient, Player> PLAYER_MAP = new ConcurrentHashMap<IGameClient, Player>();
	private static final Map<String, IGameClient> CLIENT_MAP = new ConcurrentHashMap<String, IGameClient>();
	private static final Random RANDOM = new Random();
	private static int  initialX = -1;
	private static int initialY = -1;

	protected GameServer()  throws RemoteException {
		super();
	}

	@Override
	public void login(IGameClient client,String name) throws RemoteException {
		Player player = new Player(name);
		PLAYER_MAP.put(client, player);
		CLIENT_MAP.put(name, client);
		sendInitialFlyPosition(client);
//		return 
	}

	public void sendInitialFlyPosition(IGameClient client){
		if(initialX == -1 && initialY == -1){
			initialX = getBoundedValue();
			initialY = getBoundedValue();
		}
		try {
			client.refreshFlyPosition(initialX, initialY);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void huntFly(String player) throws RemoteException {
		incrementScore(PLAYER_MAP.get(CLIENT_MAP.get(player)));
		updateScores();
		sendNewPositions();
	}


	@Override
	public void logout(String player) throws RemoteException {
		PLAYER_MAP.remove(CLIENT_MAP.remove(player));
		
	}

	private int getBoundedValue(){
		return RANDOM.nextInt(350);
	}

	private void incrementScore(Player player){
		player.incrementScore();
	}

	public void sendNewPositions(){
		int x = initialX =  getBoundedValue();
		int y = initialY = getBoundedValue();
		for(IGameClient client : PLAYER_MAP.keySet()){
			try {
				client.refreshFlyPosition(x, y);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	private void updateScores(){
		for(IGameClient client : PLAYER_MAP.keySet()){
			try {
				client.refreshScore(new ArrayList<Player>(PLAYER_MAP.values()));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

}
