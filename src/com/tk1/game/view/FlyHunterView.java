package com.tk1.game.view;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.tk1.game.controller.FlyHunterController;
import com.tk1.game.model.CoordinateModel;
import com.tk1.game.model.PlayerModel;
import com.tk1.game.vo.Coordinate;
import com.tk1.game.vo.Player;


public class FlyHunterView extends JFrame implements Observer{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String TITLE= "Fly Hunter 1.0";
	private static final String Fly_IMAGE_SOURCE = "resources/fly.jpg";

	private final StringBuffer scoreText = new StringBuffer();
	private JTextField playerName;
	private JTextArea liveScoreArea;
	private JButton flyButton; 
	private JButton connectButton;
	
	public FlyHunterView(){
		super(TITLE);
		setLocation(400, 80);
		initUI();
	}

	private void initUI(){
		setSize(400,700);
		setResizable(false);
		getContentPane().setLayout(null);
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(6, 6, 388, 388);
		getContentPane().add(panel);
		panel.setLayout(null);

		flyButton = new JButton();
		flyButton.setIcon(new ImageIcon(Fly_IMAGE_SOURCE));;
		flyButton.setBounds(89, 167, 50, 50);
		flyButton.setVisible(false);
		panel.add(flyButton);

		playerName = new JTextField();
		playerName.setBounds(6, 424, 134, 28);
		getContentPane().add(playerName);
		playerName.setColumns(10);

		connectButton = new JButton("Connect");
		connectButton.setBounds(148, 425, 117, 29);
		connectButton.setActionCommand("connect");
		getContentPane().add(connectButton);

		JLabel lblEnterPlayerName = new JLabel("Enter Player name to connect");
		lblEnterPlayerName.setBounds(6, 406, 229, 16);
		getContentPane().add(lblEnterPlayerName);

		JLabel lblLiveScore = new JLabel("Live Score");
		lblLiveScore.setBounds(6, 464, 101, 16);
		getContentPane().add(lblLiveScore);

		liveScoreArea = new JTextArea();
		liveScoreArea.setText("Not Connected...");
		liveScoreArea.setBackground(Color.BLACK);
		liveScoreArea.setForeground(Color.RED);
		liveScoreArea.setWrapStyleWord(true);
		liveScoreArea.setEditable(false);
		liveScoreArea.setBounds(6, 493, 388, 168);
		getContentPane().add(liveScoreArea);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof CoordinateModel){
			if(null!=arg && arg instanceof Coordinate){
				updateFlyLocation((Coordinate)arg);
			}
		}

		if(o instanceof PlayerModel){
			if(null!=arg && arg instanceof List){
				updateScore((List<Player>)arg);
			}
		}

	}

	private void updateFlyLocation(Coordinate coordinate){
		flyButton.setLocation(coordinate.getX(),coordinate.getY());
	}

	private void updateScore(List<Player> players){
		scoreText.setLength(0);
		for (Player player : players) {
			scoreText.append("Player: " + player.getPlayerName() + "  Score: " + player.getScore());
			scoreText.append("\n");
		}
		liveScoreArea.setText(scoreText.toString());
	}

	public void showInConsole(String message){
		liveScoreArea.setText(message);
	}

	public String getPlayerName(){
		return this.playerName.getText();
	}

	public Coordinate getFlyCoordinates(){
		return new Coordinate(flyButton.getX(), flyButton.getY());
	}

	public void showMessage(String message){
		JOptionPane.showMessageDialog(this, message);
	}

	public void setConnectButtonText(String text){
		this.connectButton.setText(text);
	}

	public void disablePlayerNameInput(){
		this.playerName.setEditable(false);
	}

	public void enablePlayerNameInput(){
		this.playerName.setEditable(true);
	}

	public void setConnectButtonActionCommand(String command){
		this.connectButton.setActionCommand(command);
	}

	public void setFlyVisibility(boolean visible){
		this.flyButton.setVisible(visible);
	}

	public void addHandlers(FlyHunterController flyHunterController){
		this.addWindowListener(flyHunterController.getWindowCloseHandler()); 
		this.flyButton.addActionListener(flyHunterController.getFlyHuntAction());
		this.connectButton.addActionListener(flyHunterController.getConnectButtonAction());
	}
}
