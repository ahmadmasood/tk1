# TK1 First Programming assignment  #

This Repo contains code of TK1 First assignment. Its a multi player game, front end is written in Java Swing and backend is on RMI (Java Remote Method Invocation)

# How to setup? #

* Clone this project (git clone <repo path>)
* In root directory run "ant run"

It will start two clients and one server.

# Group Members

### Khurram Mehmood Cheema ###
Matriculation number: 2239329

### Muhammad Bin Javaid Nasir ###
Matriculation number: 2281775

### Ahmad Masood Salahuddin ###
Matriculation number: 2282460

### Muhammad Nouman ###
Matriculation number: 2680952

 